@echo off

IF NOT EXIST ".\bin\" (
	mkdir bin
)

gcc "src\acefdp.c" -o "bin\acefdp"