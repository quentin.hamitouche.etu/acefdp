#include <stdio.h>
#include <unistd.h>

#ifdef WIN32
#include <windows.h>
#endif

#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_RESET "\x1b[0m"


void readWord(FILE *file) {
	fseek(file, 4, 1);

	int textSz;

	fread(&textSz, 4, 1, file);

	char word[textSz];
	fgets(word, textSz + 1, file);
	
	int fillable;
	fread(&fillable, 4, 1, file);
	
	if (fillable) {
		fseek(file, 8, 1);

		int spellCnt;
		fread(&spellCnt, 4, 1, file);

		fseek(file, 24, 1);
		
		if (spellCnt > 1) {
			printf(ANSI_COLOR_GREEN);

			int spellingTxtSz;

			for (int i = 0 ; i < spellCnt ; i++) {
				fseek(file, 8, 1);
				
				fread(&spellingTxtSz, 4, 1, file); // SpellingEntry
				
				fseek(file, 4 + spellingTxtSz, 1); // skip padding
			}
		} else {
			if (spellCnt) {
				fseek(file, 8, 1);
			}
		}

		// AllowedAnswer
		int hasNext, answerTxtSz;

		do {
			fread(&answerTxtSz, 4, 1, file);
			fseek(file, answerTxtSz, 1);
			fread(&hasNext, 4, 1, file);
		} while (hasNext);

	}

	if (word == NULL) {
		printf(ANSI_COLOR_RESET);
	} else {
		printf("%s" ANSI_COLOR_RESET, word);
	}



};

void processFile(char *path) {
	FILE *file = fopen(path, "rb");

	printf("\n");

	while (!feof(file)) {
		readWord(file);
	}
	
	printf("\n");

	fclose(file);
}


int main(int argc, char *argv[]) {
 	if (argc < 2) {
	 	printf("Usage: acefdp <file>\n");

		return 0;
	}

	if (access(argv[1], R_OK) == 0) { // file exists
		#ifdef WIN32
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleMode(hConsole, ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
		#endif
		
		processFile(argv[1]);
	} else {
	 	printf("Failed to open %s\n", argv[1]);
	}

	return 0;
}