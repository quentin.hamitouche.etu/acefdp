Ace File DumPer
===============

Simple programme permettant d'afficher les textes à trous de fichiers `.ace` complétés.

Je me dédouane de toute responsabilité en cas de triche. À utiliser à vos risques et périls :)

<br/>

# Utilisation d'AceFDP

Afin d'utiliser le projet, il suffit de taper les commandes suivantes dans un terminal :

```
.\compile.bat
```
Permet la compilation des fichiers présents dans `src` et la création du fichier binaire `bin/acefdp.exe`

*Note : GCC est nécessaire pour compiler le programme à cause de la dépendance de `unistd.h`.*

```
acefdp.exe <chemin vers le fichier TEXTOBJECTS d'un fichier .ace>
```

Permet de lancer le programme.

### **Si vous ne pouvez pas compiler le programme, vous pouvez télécharger le fichier binaire dans l'onglet [Releases](https://gitlab.univ-lille.fr/quentin.hamitouche.etu/acefdp/-/releases).**

<br/>

# Démonstration

![Une capture d'écran représentant le fonctionnement du programme](shots/shot1.png)

# Crédits

Développé par Quentin Hamitouche

Contact : [quentin.hamitouche.etu@univ-lille.fr](mailto:quentin.hamitouche.etu@univ-lille.fr)